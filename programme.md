# Cours J1 (9h)

- Intro (1h30) 
- Culture Générale JV + intro GD (1h30)
- intro GD + Brainstorming (1h30)
---
- Projet : Getting started with Unity (1h30)
- Les notions essentielles : Une image (1h30)
- Conception et dev projet (1h30)

# Cours J2  (9h)

- Les métiers du JV (1h30)
- Projet : Conception (1h30)
- Les notions essentielles : La caméra (1h30)
---
- Les notions essentielles : Gameplay (3h)
- Conception et dev projet (1h30)

# Cours J3  (9h)

- Théorie Culture + Fiches pratiques (1h30)
- Mise en pratique (fiches pratiques, générateur de particules , POCs) (1h30)
- Projet : finalisation de la conception itération 1 (1h30)
---
- Les notions essentielles : La physique 2D / 3D (1h30)
- Les notions essentielles : Effets spéciaux (1h30)
- Projet : finalisation de la conception itération 2 (1h30)

# Cours J4  (9h)

- Les notions essentielles : IA (1h30)
- Mise en pratique (IA pathfinding, POCs) (1h30)
- Projet : finalisation de la conception itération 3 (1h30)


# Cours J5  (9h)

- Les notions essentielles : Flot de production (1h30)
- Projet : paramétrage de l'export du projet pour la production et optimisation des builds (1h30)
- Projet (1h30)
---
- Difficultés spécifiques : multi plateformes, localisation, performances, sécurité (1h30)
- Projets (1h30)
- Projets (1h30)

# Cours J6  (9h)

- Les notions essentielles : Réseau (1h30)
- Exercice pratique (BallGame HLAPI) (1h30)
- Projets (1h30)
---
- préparer une session de playtests (1h30)
- Séances de tests cross équipes (1h30)
    - qualité intra-équipe
    - test de jeu inter-équipe
- Projets (1h30)

# Cours J7 (9h)

- Projets (1h30)
- Projets (1h30)
- Projets (1h30)
---
- GameDesign : Playtests ! (1h30)
- GameDesign : Compte rendu et analyse des playtests (1h30)
- Détails techniques :
    Physique : Réagir dynamiquement aux collisions (prendre en compte l'intensité et l'angle)
    Perfs : - Implémenter un Pool d'objet
            - Pas d'allocation mémoire pendant la partie

# Cours J8 (9h)

- Les notions essentielles : Rendu 3D (1h30)
    - Maillage
    - Matériaux
    - Textures
    - Lumière
    - Shaders
    - Exercices :
        - Programmer un shader (Potion => coloriser et animer)
- Projet (1h30)  
- Projet (1h30)  
---
- Les notions essentielles : Mathématiques (1h30)
    - la base 16
    - Vecteurs dans un espace 2D
    - Vecteurs dans un espace 3D
    - Les matrices TRS et représentation d’une scène en 3D
    - Les quaternions et leur utilisation pour les rotations
    - Les splines cubiques
    - Sphère, plan, frustrum, test AABB, triangle
    - Lancer de rayon
    - Les fractales
    - Les bruits (perlin…) et la Génération Procédurale ?
    - Probabilités (jeux de cartes…)
- Projet (1h30)
- Projet (1h30)

# Cours J8  (9h)

Pipeline de rendu + Shaders ?
Eclairage ?
Canvas UI ?
Perfs / profiling ?
Génération Procédurale ?
Animation Squelettale
Font rendering


#### Les sprites (spritesheet)
| Program Name                      | Price                                    | Type                   |
|-----------------------------------|------------------------------------------|------------------------|
|                                   |                                          |                        |
| Spine                             | Paid ($69 Essentials, $299 Professional) | Standalone program     |
| Puppet2D                          | Paid ($30)                               | Unity Editor Extension |
| DragonBones Pro                   | Free                                     | Standalone program     |
| Nima                              | Free while in beta, paid options later   | Browser-based program  |
| Anima2D                           | Free                                     | Unity Editor Extension |
| Cutout Animation Tool for Blender | Free                                     | Blender Extension      |
| Spriter                           | $59.99                                   | Standalone program     |
| Unity Sprites and Bones           | Free                                     | Unity Editor Extension |
#### Le texte
+++
No cross platform problems
No need to load the TTF/OTF file on every platform

Consisten look on every platform
Because sprites do look the same on every platform

No font licensing issues
You do not have to distribute the TTF/OTF font file with your app

More flexible
Since you can control every single character

Better performance
Regarding to 71square?

---
More work to do
Generating a bitmap font and implementing a library to load it

Scaling is limited
Because pixel images can not be scaled very large or very small
